package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html/template"
	"os"
)

type CycloneDXJson struct {
	BomFormat  string `json:"bomFormat"`
	Components []struct {
		Bom_ref            string `json:"bom-ref"`
		Cpe                string `json:"cpe"`
		Description        string `json:"description"`
		ExternalReferences []struct {
			Comment string `json:"comment"`
			Type    string `json:"type"`
			Url     string `json:"url"`
		} `json:"externalReferences"`
		Licenses []struct {
			License struct {
				Id string `json:"id"`
			} `json:"license"`
		} `json:"licenses"`
		Name       string `json:"name"`
		Properties []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"properties"`
		Publisher string `json:"publisher"`
		Purl      string `json:"purl"`
		Swid      struct {
			Name    string `json:"name"`
			TagId   string `json:"tagId"`
			Version string `json:"version"`
		} `json:"swid"`
		Type    string `json:"type"`
		Version string `json:"version"`
	} `json:"components"`
	Metadata struct {
		Component struct {
			Bom_ref string `json:"bom-ref"`
			Name    string `json:"name"`
			Type    string `json:"type"`
			Version string `json:"version"`
		} `json:"component"`
		Timestamp string `json:"timestamp"`
		Tools     []struct {
			Name    string `json:"name"`
			Vendor  string `json:"vendor"`
			Version string `json:"version"`
		} `json:"tools"`
	} `json:"metadata"`
	SerialNumber string `json:"serialNumber"`
	SpecVersion  string `json:"specVersion"`
	Version      int    `json:"version"`
}

type CycloneDXXml struct {
	XMLName      xml.Name `xml:"bom"`
	Text         string   `xml:",chardata"`
	Xmlns        string   `xml:"xmlns,attr"`
	SerialNumber string   `xml:"serialNumber,attr"`
	Version      string   `xml:"version,attr"`
	Metadata     struct {
		Text      string `xml:",chardata"`
		Timestamp string `xml:"timestamp"`
		Tools     struct {
			Text string `xml:",chardata"`
			Tool struct {
				Text    string `xml:",chardata"`
				Vendor  string `xml:"vendor"`
				Name    string `xml:"name"`
				Version string `xml:"version"`
			} `xml:"tool"`
		} `xml:"tools"`
		Component struct {
			Text    string `xml:",chardata"`
			BomRef  string `xml:"bom-ref,attr"`
			Type    string `xml:"type,attr"`
			Name    string `xml:"name"`
			Version string `xml:"version"`
		} `xml:"component"`
	} `xml:"metadata"`
	Components struct {
		Text      string `xml:",chardata"`
		Component []struct {
			Text      string `xml:",chardata"`
			BomRef    string `xml:"bom-ref,attr"`
			Type      string `xml:"type,attr"`
			Publisher string `xml:"publisher"`
			Name      string `xml:"name"`
			Version   string `xml:"version"`
			Licenses  struct {
				Text    string `xml:",chardata"`
				License []struct {
					Text string `xml:",chardata"`
					ID   string `xml:"id"`
					Name string `xml:"name"`
				} `xml:"license"`
			} `xml:"licenses"`
			Cpe        string `xml:"cpe"`
			Purl       string `xml:"purl"`
			Properties struct {
				Text     string `xml:",chardata"`
				Property []struct {
					Text string `xml:",chardata"`
					Name string `xml:"name,attr"`
				} `xml:"property"`
			} `xml:"properties"`
			Description string `xml:"description"`
			Swid        struct {
				Text    string `xml:",chardata"`
				TagId   string `xml:"tagId,attr"`
				Name    string `xml:"name,attr"`
				Version string `xml:"version,attr"`
			} `xml:"swid"`
			ExternalReferences struct {
				Text      string `xml:",chardata"`
				Reference []struct {
					Text    string `xml:",chardata"`
					Type    string `xml:"type,attr"`
					URL     string `xml:"url"`
					Comment string `xml:"comment"`
				} `xml:"reference"`
			} `xml:"externalReferences"`
		} `xml:"component"`
	} `xml:"components"`
}

func NewCycloneDXJson(stream []byte) (CycloneDXJson, error) {

	var cyclonedx CycloneDXJson

	if err := json.Unmarshal(stream, &cyclonedx); err != nil {
		return CycloneDXJson{}, err
	}

	return cyclonedx, nil
}

func NewCycloneDXXml(stream []byte) (CycloneDXXml, error) {

	var cyclonedx CycloneDXXml

	if err := xml.Unmarshal(stream, &cyclonedx); err != nil {
		return CycloneDXXml{}, err
	}

	return cyclonedx, nil
}

func (c *CycloneDXXml) Process(conf Config) error {
	// Create the routes file
	fmt.Println("Writing report to:", conf.OutputFile)
	f, err := os.Create(conf.OutputFile)

	if err != nil {
		return err
	}

	// Create the routes template and execute on the definer
	return template.Must(template.New("cyclonedx").Parse(c.Template())).Execute(f, c)
}

func (c *CycloneDXJson) Process(conf Config) error {
	// Create the routes file
	fmt.Println("Writing report to:", conf.OutputFile)
	f, err := os.Create(conf.OutputFile)

	if err != nil {
		return err
	}

	// Create the routes template and execute on the definer
	return template.Must(template.New("cyclonedx").Parse(c.Template())).Execute(f, c)
}

func (c *CycloneDXJson) Template() string {
	return `<!DOCTYPE html>
<html>
	<head>
	<title>{{ .BomFormat }}</title>
	</head>
	<body>
	<p>
		Specification Version: {{ .SpecVersion }}
	</p>
	<p>
		Serial Number: {{ .SerialNumber }}
	</p>
	<p>
		Version: {{ .Version }}
	</p>
	<!-- CSS Code: Place this code in the document's head (between the 'head' tags) -->

	<!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
	<table class="GeneratedTable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Version</th>
			<th>Publisher</th>
		</tr>
	</thead>
	<tbody>
		{{ range .Components}}
		<tr>
			<td>{{ .Name }}</td>
			<td>{{ .Version }}</td>
			<td>{{ .Publisher }}</td>
		</tr>
		{{ end }}
	</tbody>
	</table>
	</body>
</html>`
}

func (c *CycloneDXXml) Template() string {
	return `<!DOCTYPE html>
<html>
	<head>
	<title>{{ .XMLName }}</title>
	</head>
	<body>
	<p>
		Version: {{ .Version }}
	</p>
	<!-- CSS Code: Place this code in the document's head (between the 'head' tags) -->

	<!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
	<table class="GeneratedTable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Version</th>
			<th>Publisher</th>
		</tr>
	</thead>
	<tbody>
		{{ range .Components.Component }}
		<tr>
			<td>{{ .Name }}</td>
			<td>{{ .Version }}</td>
			<td>{{ .Publisher }}</td>
		</tr>
		{{ end }}
	</tbody>
	</table>
	</body>
</html>`
}
