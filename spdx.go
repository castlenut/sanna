package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"os"
)

type SPDX struct {
	SPDXID       string `json:"SPDXID"`
	CreationInfo struct {
		Created            string   `json:"created"`
		Creators           []string `json:"creators"`
		LicenseListVersion string   `json:"licenseListVersion"`
	} `json:"creationInfo"`
	DataLicense       string `json:"dataLicense"`
	DocumentNamespace string `json:"documentNamespace"`
	Files             []struct {
		SPDXID    string `json:"SPDXID"`
		Checksums []struct {
			Algorithm     string `json:"algorithm"`
			ChecksumValue string `json:"checksumValue"`
		} `json:"checksums"`
		Comment          string `json:"comment"`
		CopyrightText    string `json:"copyrightText"`
		FileName         string `json:"fileName"`
		LicenseConcluded string `json:"licenseConcluded"`
	} `json:"files"`
	HasExtractedLicensingInfos []struct {
		ExtractedText string `json:"extractedText"`
		LicenseId     string `json:"licenseId"`
		Name          string `json:"name"`
	} `json:"hasExtractedLicensingInfos"`
	Name     string `json:"name"`
	Packages []struct {
		SPDXID    string `json:"SPDXID"`
		Checksums []struct {
			Algorithm     string `json:"algorithm"`
			ChecksumValue string `json:"checksumValue"`
		} `json:"checksums"`
		CopyrightText    string `json:"copyrightText"`
		DownloadLocation string `json:"downloadLocation"`
		ExternalRefs     []struct {
			ReferenceCategory string `json:"referenceCategory"`
			ReferenceLocator  string `json:"referenceLocator"`
			ReferenceType     string `json:"referenceType"`
		} `json:"externalRefs"`
		LicenseConcluded string `json:"licenseConcluded"`
		LicenseDeclared  string `json:"licenseDeclared"`
		Name             string `json:"name"`
		Originator       string `json:"originator"`
		SourceInfo       string `json:"sourceInfo"`
		VersionInfo      string `json:"versionInfo"`
	} `json:"packages"`
	Relationships []struct {
		RelatedSpdxElement string `json:"relatedSpdxElement"`
		RelationshipType   string `json:"relationshipType"`
		SpdxElementId      string `json:"spdxElementId"`
	} `json:"relationships"`
	SpdxVersion string `json:"spdxVersion"`
}

func NewSPDX(stream []byte) (SPDX, error) {

	var spdx SPDX

	if err := json.Unmarshal(stream, &spdx); err != nil {
		return SPDX{}, err
	}

	return spdx, nil
}

func (s *SPDX) Process(conf Config) error {
	// Create the routes file
	fmt.Println("Writing report to:", conf.OutputFile)
	f, err := os.Create(conf.OutputFile)

	if err != nil {
		return err
	}

	// Create the routes template and execute on the definer
	return template.Must(template.New("spdx").Parse(s.Template())).Execute(f, s)
}

func (s *SPDX) Template() string {
	return `<!DOCTYPE html>
<html>
	<head>
	<title>{{ .Name }}</title>
	</head>
	<body>
	<p>
		Specification Version: {{ .SpdxVersion }}
	</p>
	<p>
		Data License: {{ .DataLicense }}
	</p>
	<p>
		Name: {{ .Name }}
	</p>
	<!-- CSS Code: Place this code in the document's head (between the 'head' tags) -->

	<!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
	<table class="GeneratedTable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Version Info</th>
			<th>Originator</th>
		</tr>
	</thead>
	<tbody>
		{{ range .Packages}}
		<tr>
			<td>{{ .Name }}</td>
			<td>{{ .VersionInfo }}</td>
			<td>{{ .Originator }}</td>
		</tr>
		{{ end }}
	</tbody>
	</table>
	</body>
</html>`
}
