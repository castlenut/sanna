package main

import (
	"fmt"
	"io"
	"log"
	"os"

	cli "github.com/urfave/cli/v2"
)

var ()

func main() {

	fmt.Println(`
   ___       ___       ___       ___       ___   
  /\  \     /\  \     /\__\     /\__\     /\  \  
 /::\  \   /::\  \   /:| _|_   /:| _|_   /::\  \ 
/\:\:\__\ /::\:\__\ /::|/\__\ /::|/\__\ /::\:\__\
\:\:\/__/ \/\::/  / \/|::/  / \/|::/  / \/\::/  /
 \::/  /    /:/  /    |:/  /    |:/  /    /:/  / 
  \/__/     \/__/     \/__/     \/__/     \/__/  
	`)

	fmt.Printf("\x1b[1m\x1b[4mVersion:\x1b[0m %s\n\n", version)

	conf, err := NewConfig()

	if err != nil {
		panic(err)
	}

	app := &cli.App{
		Name:  conf.AppName,
		Usage: conf.AppDesc,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "file",
				Required: false,
				Aliases:  []string{"f"},
				Usage:    "File to read",
			},
			&cli.StringFlag{
				Name:     "output",
				Required: false,
				Aliases:  []string{"o"},
				Usage:    "Output file name",
			},
			&cli.StringFlag{
				Name:     "sbom-format",
				Required: true,
				Aliases:  []string{"s"},
				Usage:    "Format of the input SBOM file. Can be one of 'cyclonedxjson', 'cyclonedxxml', 'spdx' or 'syft'",
			},
		},
		Action: func(cCtx *cli.Context) error {

			var sanna_input []byte

			if cCtx.String("output") != "" {
				conf.OutputFile = cCtx.String("output")
			}

			if cCtx.String("template") != "" {
				conf.Template = cCtx.String("template")
			}

			if cCtx.String("file") != "" {
				sanna_input, err = os.ReadFile(cCtx.String("file"))

				if err != nil {
					log.Fatalf("💀 \x1b[31mFailed to open file:\x1b[0m %s", err.Error())
				}
			}

			stat, err := os.Stdin.Stat()

			if err != nil {
				panic(err)
			}

			if (stat.Mode() & os.ModeCharDevice) == 0 {
				sanna_input, err = io.ReadAll(os.Stdin)

				if err != nil {
					panic(err)
				}
			}

			switch cCtx.String("sbom-format") {
			case "cyclonedxjson":
				if cyclone_json, err := NewCycloneDXJson(sanna_input); err == nil {
					if err := cyclone_json.Process(conf); err != nil {
						log.Fatalf("💀 \x1b[31mFailed to process cyclonedx input:\x1b[0m %s", err.Error())
					}
				}
			case "cyclonedxxml":
				if cyclone_xml, err := NewCycloneDXXml(sanna_input); err == nil {
					if err := cyclone_xml.Process(conf); err != nil {
						log.Fatalf("💀 \x1b[31mFailed to process cyclonedx input:\x1b[0m %s", err.Error())
					}
				}
			case "spdx":
				if spdx, err := NewSPDX(sanna_input); err == nil {
					if err := spdx.Process(conf); err != nil {
						log.Fatalf("💀 \x1b[31mFailed to process spdx input:\x1b[0m %s", err.Error())
					}
				}
			case "syft":
				if syft, err := NewSyft(sanna_input); err == nil {
					if err := syft.Process(conf); err != nil {
						log.Fatalf("💀 \x1b[31mFailed to process syft input:\x1b[0m %s", err.Error())
					}
				}
			default:
				log.Fatal("💀 \x1b[31mUnrecognized input\x1b[0m\n")
			}

			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalf("💀 %s", err.Error())
	}
}
