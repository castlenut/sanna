package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"os"
)

type Syft struct {
	ArtifactRelationships []struct {
		Child  string `json:"child"`
		Parent string `json:"parent"`
		Type   string `json:"type"`
	} `json:"artifactRelationships"`
	Artifacts []struct {
		Cpes      []string `json:"cpes"`
		FoundBy   string   `json:"foundBy"`
		Id        string   `json:"id"`
		Language  string   `json:"language"`
		Licenses  []string `json:"licenses"`
		Locations []struct {
			LayerID string `json:"layerID"`
			Path    string `json:"path"`
		} `json:"locations"`
		Metadata struct {
			Architecture string `json:"architecture"`
			Files        []struct {
				Digest struct {
					Algorithm string `json:"algorithm"`
					Value     string `json:"value"`
				} `json:"digest"`
				IsConfigFile bool   `json:"isConfigFile"`
				Path         string `json:"path"`
			} `json:"files"`
			GoBuildSettings struct {
				_Compiler    string `json:"-compiler"`
				_Ldflags     string `json:"-ldflags"`
				CGO_CFLAGS   string `json:"CGO_CFLAGS"`
				CGO_CPPFLAGS string `json:"CGO_CPPFLAGS"`
				CGO_CXXFLAGS string `json:"CGO_CXXFLAGS"`
				CGO_ENABLED  string `json:"CGO_ENABLED"`
				CGO_LDFLAGS  string `json:"CGO_LDFLAGS"`
				GOAMD64      string `json:"GOAMD64"`
				GOARCH       string `json:"GOARCH"`
				GOOS         string `json:"GOOS"`
			} `json:"goBuildSettings"`
			GoCompiledVersion string `json:"goCompiledVersion"`
			H1Digest          string `json:"h1Digest"`
			InstalledSize     int    `json:"installedSize"`
			MainModule        string `json:"mainModule"`
			Maintainer        string `json:"maintainer"`
			Package           string `json:"package"`
			Source            string `json:"source"`
			SourceVersion     string `json:"sourceVersion"`
			Version           string `json:"version"`
		} `json:"metadata"`
		MetadataType string `json:"metadataType"`
		Name         string `json:"name"`
		Purl         string `json:"purl"`
		Type         string `json:"type"`
		Version      string `json:"version"`
	} `json:"artifacts"`
	Descriptor struct {
		Configuration struct {
			Attest               struct{}    `json:"attest"`
			Catalogers           interface{} `json:"catalogers"`
			Check_for_app_update bool        `json:"check-for-app-update"`
			ConfigPath           string      `json:"configPath"`
			Dev                  struct {
				Profile_cpu bool `json:"profile-cpu"`
				Profile_mem bool `json:"profile-mem"`
			} `json:"dev"`
			Exclude             []interface{} `json:"exclude"`
			File                string        `json:"file"`
			File_classification struct {
				Cataloger struct {
					Enabled bool   `json:"enabled"`
					Scope   string `json:"scope"`
				} `json:"cataloger"`
			} `json:"file-classification"`
			File_contents struct {
				Cataloger struct {
					Enabled bool   `json:"enabled"`
					Scope   string `json:"scope"`
				} `json:"cataloger"`
				Globs                 []interface{} `json:"globs"`
				Skip_files_above_size int           `json:"skip-files-above-size"`
			} `json:"file-contents"`
			File_metadata struct {
				Cataloger struct {
					Enabled bool   `json:"enabled"`
					Scope   string `json:"scope"`
				} `json:"cataloger"`
				Digests []string `json:"digests"`
			} `json:"file-metadata"`
			Log struct {
				File_location string `json:"file-location"`
				Level         string `json:"level"`
				Structured    bool   `json:"structured"`
			} `json:"log"`
			Name                 string   `json:"name"`
			Output               []string `json:"output"`
			Output_template_path string   `json:"output-template-path"`
			Package              struct {
				Cataloger struct {
					Enabled bool   `json:"enabled"`
					Scope   string `json:"scope"`
				} `json:"cataloger"`
				Search_indexed_archives   bool `json:"search-indexed-archives"`
				Search_unindexed_archives bool `json:"search-unindexed-archives"`
			} `json:"package"`
			Parallelism int    `json:"parallelism"`
			Platform    string `json:"platform"`
			Quiet       bool   `json:"quiet"`
			Registry    struct {
				Auth                     []interface{} `json:"auth"`
				Insecure_skip_tls_verify bool          `json:"insecure-skip-tls-verify"`
				Insecure_use_http        bool          `json:"insecure-use-http"`
			} `json:"registry"`
			Secrets struct {
				Additional_patterns struct{} `json:"additional-patterns"`
				Cataloger           struct {
					Enabled bool   `json:"enabled"`
					Scope   string `json:"scope"`
				} `json:"cataloger"`
				Exclude_pattern_names []interface{} `json:"exclude-pattern-names"`
				Reveal_values         bool          `json:"reveal-values"`
				Skip_files_above_size int           `json:"skip-files-above-size"`
			} `json:"secrets"`
			Verbosity int `json:"verbosity"`
		} `json:"configuration"`
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"descriptor"`
	Distro struct {
		BugReportURL    string `json:"bugReportURL"`
		HomeURL         string `json:"homeURL"`
		Id              string `json:"id"`
		Name            string `json:"name"`
		PrettyName      string `json:"prettyName"`
		SupportURL      string `json:"supportURL"`
		Version         string `json:"version"`
		VersionCodename string `json:"versionCodename"`
		VersionID       string `json:"versionID"`
	} `json:"distro"`
	Files []struct {
		Id       string `json:"id"`
		Location struct {
			LayerID string `json:"layerID"`
			Path    string `json:"path"`
		} `json:"location"`
	} `json:"files"`
	Schema struct {
		Url     string `json:"url"`
		Version string `json:"version"`
	} `json:"schema"`
	Source struct {
		Id     string `json:"id"`
		Target struct {
			Architecture string `json:"architecture"`
			Config       string `json:"config"`
			ImageID      string `json:"imageID"`
			ImageSize    int    `json:"imageSize"`
			Layers       []struct {
				Digest    string `json:"digest"`
				MediaType string `json:"mediaType"`
				Size      int    `json:"size"`
			} `json:"layers"`
			Manifest       string        `json:"manifest"`
			ManifestDigest string        `json:"manifestDigest"`
			MediaType      string        `json:"mediaType"`
			Os             string        `json:"os"`
			RepoDigests    []string      `json:"repoDigests"`
			Tags           []interface{} `json:"tags"`
			UserInput      string        `json:"userInput"`
		} `json:"target"`
		Type string `json:"type"`
	} `json:"source"`
}

func NewSyft(stream []byte) (Syft, error) {

	var syft Syft

	if err := json.Unmarshal(stream, &syft); err != nil {
		return Syft{}, err
	}

	return syft, nil
}

func (s *Syft) Process(conf Config) error {
	// Create the routes file
	fmt.Println("Writing report to:", conf.OutputFile)
	f, err := os.Create(conf.OutputFile)

	if err != nil {
		return err
	}

	// Create the routes template and execute on the definer
	return template.Must(template.New("syft").Parse(s.Template())).Execute(f, s)
}

func (s *Syft) Template() string {
	return `<!DOCTYPE html>
<html>
	<head>
	<!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
	<table class="GeneratedTable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Version</th>
			<th>Type</th>
		</tr>
	</thead>
	<tbody>
		{{ range .Artifacts}}
		<tr>
			<td>{{ .Name }}</td>
			<td>{{ .Version }}</td>
			<td>{{ .Type }}</td>
		</tr>
		{{ end }}
	</tbody>
	</table>

	</body>
</html>`
}
