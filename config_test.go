package main

func (suite *SannaTestSuite) TestNewConfig() {
	conf, err := NewConfig()
	suite.NoError(err)
	suite.IsType(Config{}, conf)
}

func (suite *SannaTestSuite) TestConfig() {
	suite.NotEmpty(suite.Conf.AppName)
	suite.Equal("development", suite.Conf.AppEnv)
}
