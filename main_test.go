package main

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type SannaTestSuite struct {
	suite.Suite
	Conf Config
}

func (suite *SannaTestSuite) SetupTest() {
	suite.Conf, _ = NewConfig()
	version = "development"
}

func (suite *SannaTestSuite) TestVersion() {
	suite.NotEqual("", version)
}

func TestSannaTestSuite(t *testing.T) {
	suite.Run(t, new(SannaTestSuite))
}
