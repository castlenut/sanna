include .env

.EXPORT_ALL_VARIABLES:;
.PHONY: default init login prereq clean build test install run pull image create destroy recreate start logs push helm_install_plugins helm_add_repo helm_update_chart helm_push_chart kubectl_config
.DEFAULT_GOAL = default

PROJECT ?= $(shell basename $$PWD)
REGISTRY_URL=registry.erm.fathom5.work
GITLAB_GROUP=erm
IMG=${PROJECT}
NAME=${PROJECT}
TAG=latest
VERSION=$(shell git describe --abbrev=0)
FLAGS = -ldflags="-X main.VERSION=$(VERSION)"

default:
	@ mmake help

# init project
init: login
	@ go mod vendor

# docker login to registry
login:
	@ echo "authenticating docker with gitlab..."
	@ echo " - use your gitlab username"
	@ echo " - use a personal access token (with api scope) for your password"
	@ docker login ${REGISTRY_URL}

# preflight checks
prereq:
	@ git rev-parse HEAD 1> /dev/null
	@ git describe --always 1> /dev/null
	@ basename `pwd` 1> /dev/null
	@ go list 1> /dev/null

# remove build assets
clean:
	@ rm -rf bin/${PROJECT}

# build app
build: | prereq clean
	CGO_ENABLED=0 go build -a -v $(FLAGS) -o bin/${PROJECT}
	@ echo built to 'bin/${PROJECT}'

# run tests
test:
	@ go test -a -v --cover $(FLAGS) ./...

# install globally
install: prereq
	@ go install $(FLAGS)
	@ echo installed to $$GOPATH/bin

# run app
run:
	@ go run $(FLAGS) .

# pull the Docker container
pull:
	@ docker pull ${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}:${TAG}

# build the Docker image for your machine
image:
	@ docker build \
		--no-cache \
		--network host \
		-t ${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}:latest \
        -t ${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}:${VERSION} \
		.

# create the container
create:
	@ docker create \
		-it \
		--name "${NAME}" \
		--env .env \
		--network host \
		${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}

# destroy the container
destroy:
	@ docker rm -v $(shell docker ps -aqf "name=${NAME}") || (exit 0)

# recreate the container
recreate: | destroy create

# start the container (must have already been created)
start:
	@ docker start -ai ${NAME}

# follow container logs
logs:
	@ docker logs -f ${NAME}

# push the container to the container registry
push: image
	@ docker push ${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}:latest
	@ docker push ${REGISTRY_URL}/${GITLAB_GROUP}/${IMG}:${VERSION}

# install helm plugins
helm_install_plugins:
	@ VERIFY_CHECKSUM=false
	@ helm plugin install https://github.com/chartmuseum/helm-push.git
	@ helm plugin install https://github.com/databus23/helm-diff

# add helm repo
helm_add_repo:
	@ helm repo add --username ${CI_REGISTRY_USER} --password ${CI_BUILD_TOKEN} erm https://gitlab.erm.fathom5.work/api/v4/projects/18/packages/helm/stable

# update helm chart with latest version values
helm_update_chart:
	@ sed -i "s/appVersion.*/appVersion: \"$(shell git describe --abbrev=0)\"/g" chart/Chart.yaml
	@ sed -i "s/tag:.*/tag: $(shell git describe --abbrev=0)/g" chart/values.yaml

# push helm chart manually
helm_push_chart: update-chart
	@ helm package ./chart
	@ helm cm-push $(helm show chart ./chart | grep name | cut -c7-)-$(helm show chart ./chart | grep version | cut -c10-).tgz erm
	@ helm upgrade --install ${PROJECT} ./chart --namespace default

# kubectl config
kubectl_config:
	@ kubectl config set-context default --namespace=NAMESPACE
	@ if [ ! -d "${HOME}/.kube" ]; then mkdir "${HOME}/.kube"; fi
	@ chmod g-r ~/.kube/config
	@ cat ${HOME}/.kube/config

%:
	@ true
