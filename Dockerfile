FROM golang:1.20.2-alpine3.17 AS builder

WORKDIR /src/sanna

COPY . /src/sanna
    
RUN CGO_ENABLED=0 go build -a -v -ldflags="-X main.VERSION=$(git describe --abbrev=0)" -o bin/sanna

FROM scratch

WORKDIR /

COPY --from=builder /src/sanna/bin/sanna ./sanna

ENTRYPOINT ["./sanna"]
