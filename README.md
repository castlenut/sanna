# Sanna

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/castlenut/sanna)](https://goreportcard.com/report/gitlab.com/castlenut/sanna)

Sanna is a command-line interface tool that enables conversion of common software bill of material formats to HTML reports.

## Supported Formats

- CycloneDX JSON
- CycloneDX XML
- SPDX (Software Package Data Exchange)
- Syft

## Installation

You can download and install Sanna by using the following command:

```bash
go install gitlab.com/castlenut/sanna@latest
```

## Usage

To use Sanna, you need to have a supported software bill of material file in any of the supported formats mentioned above.

### Command-Line Interface

```bash
sanna --help

   ___       ___       ___       ___       ___   
  /\  \     /\  \     /\__\     /\__\     /\  \  
 /::\  \   /::\  \   /:| _|_   /:| _|_   /::\  \ 
/\:\:\__\ /::\:\__\ /::|/\__\ /::|/\__\ /::\:\__\
\:\:\/__/ \/\::/  / \/|::/  / \/|::/  / \/\::/  /
 \::/  /    /:/  /    |:/  /    |:/  /    /:/  / 
  \/__/     \/__/     \/__/     \/__/     \/__/  

Version: vX.X.X

NAME:
   sanna - Convert popular SBOM formats into HTML reports

USAGE:
   sanna [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --file value, -f value         File to read
   --output value, -o value       Output file name
   --sbom-format value, -s value  Format of the input SBOM file. Can be one of 'cyclonedxjson', 'cyclonedxxml', 'spdx' or 'syft'
   --help, -h                     show help
```

Basic usage: 

```bash
sanna -f <file> --sbom-format <value> [options]
```

The file argument should be the path to your software bill of material file and the sbom-format should be one of the following SBOM formats:

- cyclonedxjson
- cyclonedxxml
- spdx
- syft

You can add options to the command to configure the output of the HTML report. The following options are available:

- `-o, --output`: Specifies the output file name (default is `index.html`)

Here is an example of how to run Sanna:

```bash
sanna --file /path/to/bom/file --sbom-format spdx
```

You can also pipe the output of the software bill of material file into Sanna:

```
cat /path/to/bom/file | sanna --sbom-format cyclonedxjson
```

## Contributing

We welcome contributions from anyone who is interested in improving Sanna. If you would like to contribute, please fork the project and submit a pull request.

## License

Sanna is released under the MIT License.